const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

// Configurar el motor de vistas EJS
app.set('view engine', 'ejs');

// Middleware para manejar datos URL-encoded
app.use(bodyParser.urlencoded({ extended: true }));

// Servir archivos estáticos desde el directorio 'public'
app.use(express.static(__dirname + '/public'));

// Ruta principal
app.get('/', (req, res) => {
    res.render('index', { titulo: "Mi Primer Pagina en ejs", nombre: "Yohan Alek Plazola Arangure" });
});

// Ruta para la tabla (GET y POST)
app.get('/tabla', (req, res) => {
    const numero = req.query.numero ; 
    res.render('tabla', { numero: numero });
});

app.post('/tabla', (req, res) => {
    const numero = req.body.numero ;
    res.render('tabla', { numero: numero });
});

// Iniciar el servidor
app.listen(port, () => {
    console.log("Iniciando el servidor en el puerto " + port);
});